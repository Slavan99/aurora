<?php

namespace App\Form;

use App\Entity\Event;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\EventType;
use Symfony\Component\Validator\Constraints\File;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('last_name')
            ->add('name')
            ->add('patron_name')
            ->add('phone')
            ->add('city')
            ->add('description')
            ->add('events',EntityType::class, [
                'label' => 'События',
                'class' => Event::class,
                'expanded' => true,
                'multiple' => true,
                'by_reference' => false
            ])
            ->add('interests', EntityType::class, [
                'label' => 'Интересы',
                'class' => EventType::class,
                'expanded' => true,
                'multiple' => true,
                'by_reference' => false
            ])
            ->add('age')
            ->add('photo', FileType::class, [
                'label' => 'Photo (JPG file)',

                // unmapped means that this field is not associated to any entity property
                'mapped' => false,

                // make it optional so you don't have to re-upload the PDF file
                // every time you edit the Product details
                'required' => false,

                // unmapped fields can't define their validation using annotations
                // in the associated entity, so you can use the PHP constraint classes
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'application/jpg',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid JPG',
                    ])
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
