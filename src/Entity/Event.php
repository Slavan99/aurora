<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EventRepository")
 */
class Event
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("event_show")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"event_show", "event_create"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"event_show", "event_create"})
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"event_show", "event_create"})
     */
    private $datetime;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"event_show", "event_create"})
     */
    private $place;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"event_show", "event_create"})
     */
    private $min_age;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\EventType", inversedBy="events")
     * @ORM\JoinColumn(nullable=false)
     *
     */
    private $type;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"event_show", "event_create"})
     */
    private $min_users;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"event_show", "event_create"})
     */
    private $max_users;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="events")
     * @ORM\JoinColumn(nullable=false)
     *  @Groups({"event_show", "event_create"})
     */
    private $organizer;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="events")
     * @Groups({"event_show", "event_create"})
     */
    private $participants;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="event")
     * @Groups({"event_show", "event_create"})
     */
    private $comments;

    /**
     * @ORM\Column(type="array")
     * @Groups({"event_show", "event_create"})
     */
    private $photos = [];

    public function __construct()
    {
        $this->participants = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDatetime(): ?\DateTimeInterface
    {
        return $this->datetime;
    }

    public function setDatetime(\DateTimeInterface $datetime): self
    {
        $this->datetime = $datetime;

        return $this;
    }

    public function getPlace(): ?string
    {
        return $this->place;
    }

    public function setPlace(string $place): self
    {
        $this->place = $place;

        return $this;
    }

    public function getMinAge(): ?int
    {
        return $this->min_age;
    }

    public function setMinAge(int $min_age): self
    {
        $this->min_age = $min_age;

        return $this;
    }

    public function getType(): ?EventType
    {
        return $this->type;
    }

    public function setType(?EventType $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getMinUsers(): ?int
    {
        return $this->min_users;
    }

    public function setMinUsers(int $min_users): self
    {
        $this->min_users = $min_users;

        return $this;
    }

    public function getMaxUsers(): ?int
    {
        return $this->max_users;
    }

    public function setMaxUsers(int $max_users): self
    {
        $this->max_users = $max_users;

        return $this;
    }

    public function getOrganizer(): ?User
    {
        return $this->organizer;
    }

    public function setOrganizer(?User $organizer): self
    {
        $this->organizer = $organizer;

        $this->addParticipant($organizer);

        return $this;

    }

    /**
     * @return Collection|User[]
     */
    public function getParticipants(): Collection
    {
        return $this->participants;
    }

    public function addParticipant(User $participant): self
    {
        if (!$this->participants->contains($participant)) {
            $this->participants[] = $participant;
        }

        return $this;
    }

    public function removeParticipant(User $participant): self
    {
        if ($this->participants->contains($participant)) {
            $this->participants->removeElement($participant);
        }

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setEvent($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getEvent() === $this) {
                $comment->setEvent(null);
            }
        }

        return $this;
    }

    public function getPhotos(): ?array
    {
        return $this->photos;
    }

    public function setPhotos(array $photos): self
    {
        $this->photos = $photos;

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }
}
