<?php


namespace App\Controller\Api;

use App\Entity\Event;
use App\Repository\CommentRepository;
use App\Repository\EventRepository;
use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/comment")
 */
class CommentController extends AbstractController
{
    /**
     * @Route("/", name="api_comment_index", methods={"GET"})
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     * @throws \ReflectionException
     *
     * @SWG\Tag(name="Comment")
     * @SWG\Response(
     *     response=200,
     *     description="Returns Comment",
     *     @SWG\Schema(
     *          @Model(type=Comment::class, groups={"comment_show"})
     *     )
     * )
     *
     * @SWG\Parameter(
     *   name="apikey",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="auth event's apikey"
     * )
     */
    public function index(CommentRepository $commentRepository, SerializerInterface $serializer, Request $request): Response
    {
        $comments = $commentRepository->findAll();

        $jsonComments = $serializer->serialize($comments, 'json',['groups' => ['comment_show','user_show']]);

        return new JsonResponse($jsonComments);
    }
}