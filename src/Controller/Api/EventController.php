<?php


namespace App\Controller\Api;

use App\Entity\Event;
use App\Repository\EventRepository;
use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/event")
 */
class EventController extends AbstractController
{
    /**
     * @Route("/", name="api_event_index", methods={"GET"})
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     * @throws \ReflectionException
     *
     * @SWG\Tag(name="Event")
     * @SWG\Response(
     *     response=200,
     *     description="Returns Event",
     *     @SWG\Schema(
     *          @Model(type=Event::class, groups={"event_show"})
     *     )
     * )
     *
     * @SWG\Parameter(
     *   name="apikey",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="auth event's apikey"
     * )
     */
    public function index(EventRepository $eventRepository, SerializerInterface $serializer, Request $request): Response
    {
        $events = $eventRepository->findAll();

        $jsonEvents = $serializer->serialize($events, 'json',['groups' => ['event_show', 'user_show', 'eventtype_show', 'comment_show']]);

        return new JsonResponse($jsonEvents);
    }

    /**
     * @Route("/new", name="api_event_new", methods={"POST"})
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     *
     * @SWG\Tag(name="Event")
     * @SWG\Response(
     *     response=200,
     *     description="Returns Event",
     *     @SWG\Schema(
     *          @Model(type=Event::class, groups={"event_show"})
     *     )
     * )
     *
     * @SWG\Parameter(
     *   name="apikey",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="auth user's apikey"
     * )
     *
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Create event schema.",
     *     required=true,
     *     @SWG\Schema(
     *          @Model(type=Event::class, groups={"event_create"})
     *     )
     * )
     */
    public function new(EntityManagerInterface $em, SerializerInterface $serializer, Request $request): Response
    {
        $json = $request->getContent();
        $event = $serializer->deserialize($json, Event::class, 'json', ['groups' => ['event_create']]);
        $em->persist($event);
        $em->flush();
        $jsonEvents = $serializer->serialize($event, 'json', ['groups' => ['event_show', 'user_show', 'eventtype_show', 'comment_show']]);

        return new JsonResponse($jsonEvents);

    }

}