<?php


namespace App\Controller\Api;

use App\Entity\EventType;
use App\Repository\EventTypeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;


/**
 * @Route("/eventtype")
 */
class EventTypeController extends AbstractController
{
    /**
     * @Route("/", name="api_eventtype_index", methods={"GET"})
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     * @throws \ReflectionException
     *
     * @SWG\Tag(name="EventType")
     * @SWG\Response(
     *     response=200,
     *     description="Returns EventType",
     *     @SWG\Schema(
     *          @Model(type=EventType::class, groups={"eventtype_show"})
     *     )
     * )
     *
     * @SWG\Parameter(
     *   name="apikey",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="auth eventtype's apikey"
     * )
     */
    public function index(EventTypeRepository $eventTypeRepository, SerializerInterface $serializer, Request $request): Response
    {
        $eventTypes = $eventTypeRepository->findAll();

        $jsonEventTypes = $serializer->serialize($eventTypes, 'json',['groups' => ['eventtype_show']]);

        return new JsonResponse($jsonEventTypes);
    }
}