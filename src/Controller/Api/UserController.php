<?php


namespace App\Controller\Api;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Tests\Functional\Controller\FOSRestController;
use phpDocumentor\Reflection\DocBlock\Serializer;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;


/**
 * @Route("/user")
 */

class UserController extends AbstractController
{

    /**
     * @Route("/", name="api_user_index", methods={"GET"})
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     * @throws \ReflectionException
     *
     * @SWG\Tag(name="User")
     * @SWG\Response(
     *     response=200,
     *     description="Returns User",
     *     @SWG\Schema(
     *          @Model(type=User::class, groups={"user_show"})
     *     )
     * )
     *
     * @SWG\Parameter(
     *   name="apikey",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="auth user's apikey"
     * )
     */
    public function index(UserRepository $userRepository, SerializerInterface $serializer, Request $request): Response
    {
        $users = $userRepository->findAll();
        $jsonUsers = $serializer->serialize($users, 'json', ['groups' => ['user_show']]);

        return new JsonResponse($jsonUsers);


    }




}